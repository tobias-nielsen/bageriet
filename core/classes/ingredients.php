<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 26-09-2017
 * Time: 19:29
 */

class ingredients
{
    public $db;
    public $iIngredientsID;
    public $vcTitle;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;


    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iIngredientsID" => "ID",
            "vcTitle" => "Ingredient Name",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iIngredientsID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),


        );

        $this->arrValues = array();
    }


    public function getlist() {  //function = method
        $sql = "SELECT * FROM ingredients WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single ingredients
     * @param $iIngredientsID
     */
    public function getIngredients($iIngredientsID) { //set parameter iIngredientsID to get a single ingredients
        $this->iIngredientsID = $iIngredientsID;
        $sql = "SELECT * FROM ingredients WHERE iIngredientsID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iIngredientsID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }
    }


    /**
     * @return int
     *
     */
    public function save() {
        if ($this->iIngredientsID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcTitle,
                $this->iIngredientsID,
            );

            $sql = "UPDATE ingredients SET " .
                "vcTitle = ? " .
                "WHERE iIngredientsID = ? ";

            $this->db->_query($sql, $params);
            return $this->iIngredientsID;


        } else {
            //CREATE MODE
            $params = array(
                $this->vcTitle

            );

            $sql = "INSERT INTO ingredients (" .
                "vcTitle) " .
                "VALUES(?)";
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    public function delete() {
        $params = array($this->iIngredientsID);

        $sql = "UPDATE ingredients set " .
            "iDeleted = 1 " .
            "WHERE iIngredientsID = ? ";
        $this->db->_query($sql, $params);

    }


}