<?php

class notify
{

    public function __construct() {

    }

    public static function setSession($value = 1) {
        $_SESSION["message"] = $value;
        session_write_close();
    }

    /**
     *
     * @param type $message is the message that will be displayed;
     * @param type $value is custom session value. Default set to 1;
     * @param type $type is the type of message (case sensitive), success = green / info = blue / warning = Yellow / danger = red;
     */
    public static function pushMessage($message, $value = 1, $type = "success") {

        if (!isset($_SESSION["message"])) {
            $_SESSION["message"] = 0;
        }

        if ($_SESSION["message"] === $value) {
            echo "<script type=\"text/javascript\">
           $(document).ready(function() {
               $.notify({
                   message: '" . $message . "',
                   icon: 'glyphicon glyphicon-exclamation-sign'
                   },{
                   type: '" . $type . "',
                   delay: 3500,
                   placement: {
                       from: 'top',
                       align: 'center',
                   },
                   }
               );            
           });
       </script>";

            if (isset($_SESSION["message"])) {
                unset($_SESSION["message"]);
            }
        }
    }

}

?>

<script src="/cms/assets/js/bootstrap-notify.min.js?v=<?php echo filemtime(DOCROOT . '/cms/assets/js/bootstrap-notify.min.js'); ?>"
        type="text/javascript"></script>