<?php

class news
{
    public $db;
    public $iNewsID;
    public $vcImage;
    public $vcTitle;
    public $daCreated;
    public $txDesc;
    public $iDeleted;

    public $arrLabels;
    public $arrFormElms;
    public $arrValues;



    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iNewsID" => "ID",
            "vcTitle" => "News Title",
            "vcImage" => "Image",
            "txDesc" => "Short Description",
            "daCreated" => "Created",

        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iNewsID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "vcTitle" => array("text", FILTER_SANITIZE_STRING, TRUE, "h"),
            "vcImage" => array("text", FILTER_SANITIZE_STRING, FALSE, 0),
            "txDesc" => array("textEdit", FILTER_SANITIZE_STRING, TRUE, ""),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),

        );

        $this->arrValues = array();
    }


    /**
     * function to get list of news
     * @return array
     */

    public function getList() {  //function = method
        $sql = "SELECT * FROM news WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }




    /**
     * function to get a single Event
     * @param $iNewsID
     * @return array
     */
    public function getNews($iNewsID) { //set parameter iNewsID to get a single Event
        $this->iNewsID = $iNewsID;
        $sql = "SELECT * FROM news WHERE iNewsID = ? AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iNewsID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }

    public function getAllNews($Limit) {
        $sql = "SELECT * FROM news WHERE iDeleted = 0 ORDER BY daCreated ASC LIMIT " . $Limit;
        return $this->db->_fetch_array($sql);

    }


    /**
     * Save item
     */
    public function save() {
        if ($this->iNewsID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcTitle,
                $this->vcImage,
                $this->txDesc,
                $this->daCreated,
                $this->iNewsID
            );

            $sql = "UPDATE news SET " .
                "vcTitle = ?, " .
                "vcImage = ?, " .
                "txDesc = ?, " .
                "daCreated = ? " .
                "WHERE iNewsID = ? ";

            $this->db->_query($sql, $params);
            return $this->iNewsID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcImage,
                $this->vcTitle,
                $this->txDesc,
                time()
            );

            $sql = "INSERT INTO news (" .
                "vcTitle, " .
                "vcImage, " .
                "txDesc, " .
                "daCreated) " .
                "VALUES(?,?,?,?)";
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public function delete() {
        $params = array($this->iNewsID);

        $sql = "UPDATE news SET " .
            "iDeleted = 1 " .
            "WHERE iNewsID = ? ";
        $this->db->_query($sql, $params);
    }
}