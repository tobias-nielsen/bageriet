<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 31-08-2017
 * Time: 22:13
 */

class comment
{
    public $db;
    public $iCommentID;
    public $vcName;
    public $iProductID;
    public $txContent;
    public $daCreated;
    public $iDeleted;


    public $arrLabels;
    public $arrFormElms;
    public $arrValues;

    public function __construct() {
        global $db;
        $this->db = $db;


        $this->arrLabels = array(
            "iCommentID" => "ID",
            "vcName" => "Product Name",
            "iProductID" => "Product ID",
            "daCreated" => "Created",
            "txContent" => "Short Description",
        );

        /**
         * Array for formfields:
         * Index = fieldname
         * Value[0] = formtype
         * Value[1] = filter_type
         * Value[2] = Required Status (TRUE/FALSE)
         * Value[3] = Default value
         */
        $this->arrFormElms = array(
            "iCommentID" => array("hidden", FILTER_VALIDATE_INT, FALSE, 0),
            "iProductID" => array("select", FILTER_VALIDATE_INT, FALSE,),
            "daCreated" => array("hidden", FILTER_VALIDATE_INT, FALSE, ""),
            "txContent" => array("textEdit", FILTER_SANITIZE_STRING, TRUE, ""),
            "vcName" => array("text", FILTER_SANITIZE_STRING, TRUE, ""),


        );

        $this->arrValues = array();
    }


    /**
     * function to get list of comment
     * @return array
     */

    public function getlist() {  //function = method
        $sql = "SELECT * FROM comment WHERE iDeleted = 0";

        return $this->db->_fetch_array($sql); //gets all rows, fetch value will take out a single row.

    }


    /**
     * function to get a single record
     * @param $iCommentID
     * @return array
     */
    public function getComment($iCommentID) { //set parameter iCommentID to get a single record
        $this->iCommentID = $iCommentID;
        $sql = "SELECT * FROM comment
        WHERE iCommentID = ?
        AND iDeleted = 0";
        $row = $this->db->_fetch_array($sql, array($this->iCommentID));
        foreach ($row[0] as $key => $value) {
            $this->$key = $value;
        }

        return $row;
        //showme($row);
    }

    public function listComments($iProductID, $limit)
    {

        $sql = "SELECT * FROM comment WHERE iProductID = '" . (int)$iProductID . "' LIMIT " . $limit;

        $result = $this->db->_fetch_array($sql);

        return $result;
    }

    /**
     * @param $iCommentID
     * @return array
     */
    public function productComment($iCommentID) {
        $this->iCommentID = $iCommentID;
        $sql = "SELECT c.*, p.iProductID " .
            "FROM comment c " .
            "LEFT JOIN product p " .
            "ON c.iProductID = p.iProductID " .
            "WHERE c.iProductID = ? " .
            "AND c.iDeleted = 0 " .
            "ORDER BY c.iCommentID ASC";
        if ($row = $this->db->_fetch_array($sql, array($this->iCommentID))) {
            foreach ($row[0] as $key => $value) {
                $this->$key = $value;
            }

            return $row;
        }
    }


    /**
     * Count comments per article
     * @param $iProductID
     * @return mixed
     *
     */
    public function commentCount($iProductID) {
        $sql = "SELECT COUNT(*) FROM comment WHERE iProductID = " . (int)$iProductID . " ";

        $result = $this->db->_fetch_array($sql);

        return $result[0]["COUNT(*)"];
    }

    /**
     * Save item
     */
    public
    function save() {
        if ($this->iCommentID > 0) {
            //UPDATE MODE
            $params = array(
                $this->vcName,
                $this->iProductID,
                $this->daCreated,
                $this->txContent,
            );

            $sql = "UPDATE comment SET " .
                "vcName = ?, " .
                "iProductID = ?, " .
                "daCreated = ?, " .
                "txContent = ? " .
                "WHERE iCommentID = ? ";

            $this->db->_query($sql, $params);
            return $this->iCommentID;

        } else {
            //CREATE MODE
            $params = array(
                $this->vcName,
                $this->iProductID,
                time(),
                $this->txContent,
            );

            $sql = "INSERT INTO comment (" .
                "vcName, " .
                "iProductID, " .
                "daCreated, " .
                "txContent) " .
                "VALUES(?,?,?,?)";
            //exit();
            $this->db->_query($sql, $params);

            return $this->db->_getinsertid();

        }

    }

    /**
     * Delete item
     */

    public
    function delete() {
        $params = array($this->iCommentID);

        $sql = "UPDATE comment SET " .
            "iDeleted = 1 " .
            "WHERE iCommentID = ? ";
        $this->db->_query($sql, $params);
    }
}