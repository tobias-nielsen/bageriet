<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 27-09-2017
 * Time: 09:43
 */
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";

$db = new dbconf();
$db->_connect();
$keyword = filter_input(INPUT_GET, "keyword", FILTER_SANITIZE_STRING);



$params = array(
        "%" . $keyword . "%",
        "%" . $keyword . "%",

);

$sql = "SELECT * from product " .
    "WHERE vcTitle LIKE ? " .
    "OR txDesc LIKE ? ";

    $rows = $db->_fetch_array($sql,$params);


if (!$_GET = empty($keyword) AND count($keyword)) {


?>


    <div class="container">
    <h4>Her er de resultater <?php echo count($rows) ?> der matchede din søgning <?php echo $keyword ?></h4>

    <?php
            foreach ($rows as $row) {
                ?>



                <div class="row spacing">
                    <div class="col-md-10">
                        <section class="col-xs-12 col-sm-6 col-md-12">
                            <article class="search-result row">
                                <div class="col-xs-12 col-sm-12 col-md-3">
                                    <a href="<?php echo "product.php?mode=details&iProductID=" . $row["iProductID"] ?>"
                                       title="Lorem ipsum" class="thumbnail"><img
                                                src="images/<?php echo $row['vcImage'] ?>" alt="Lorem ipsum"/></a>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-5 excerpet">
                                    <h4><strong><?php echo $row['vcTitle'] ?></strong></h4>
                                    <p><?php if (strlen($row["txDesc"]) > 50) {
                                            $row["txDesc"] = substr($row["txDesc"], 0, 250) . "...";
                                        } else {
                                            $row["txDesc"] = $row["txDesc"] . "...";
                                        }
                                        echo $row["txDesc"] ?></p>
                                </div>
                                <span class="clearfix borda"></span>
                            </article>
                        </section>

                    </div>
                </div>
                <?php
            }
        }
             else {
    ?>
                 <div class="container">

                <?php
                echo "<h1>No match found</h1>";
            }

        ?>
    </div>

    <!--  echo "<div class='row'>
        <h3>" . $row['vcTitle'] . "</h3>
        <h3>" . $row['txShortDescription'] . "</h3>
    </div>";-->
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";
