<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "contact";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iMessageID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $contact = new contact();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcName" => "contact Name",
            "txContent" => "Description",
        );

        /* Array for all contact rows */
        $arrcategories = array();

        /* List orgs and set editing options */
        foreach ($contact->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iMessageID=" . $arrValues["iMessageID"], "eye") .
                getIcon("?mode=edit&iMessageID=" . $arrValues["iMessageID"], "pencil") .
                getIcon("", "trash", "Slet contact", "remove(" . $arrValues["iMessageID"] . ")");

            /* Add value row to arrUsers */
            $arrcategories[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrcategories) */
        $p = new listPresenter($arrColumns, $arrcategories);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iMessageID = filter_input(INPUT_GET, "iMessageID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iMessageID=" . $iMessageID, "Edit contact", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $contact = new contact();
        $contact->getItem($iMessageID);


        $arrValues = get_object_vars($contact);


        $presenter = new listPresenter($contact->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iMessageID = (int)filter_input(INPUT_GET, "iMessageID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iMessageID=" . -1, "New", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $contact = new contact();

        /* Get org if state = update */
        if ($iMessageID > 0) {
            $contact->getItem($iMessageID);
        }

        /* Get property values */
        $arrValues = get_object_vars($contact);


        /* Create presenter instance and set form */
        $form = new formpresenter($contact->arrLabels, $contact->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $contact = new contact();


        foreach ($contact->arrFormElms as $field => $arrTypes) {
            $contact->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iMessageID = $contact->save();
        header("Location: ?mode=details&iMessageID=" . $iMessageID);
        break;

    case "DELETE":
        $contact = new contact();
        $contact->iMessageID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $contact->delete($iMessageID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/public_html/assets/js/ajaxFunctions.js"></script>
