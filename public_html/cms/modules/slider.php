<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 27-09-2017
 * Time: 19:19
 */

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/header.php";

$mode = setMode();

$strModuleName = "slider";

switch (strtoupper($mode)) {

    case "LIST";

        $strModuleMode = "Overview";
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iSlideID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $slider = new slider();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Image Title",
            "vcCaption" => "Caption",
            "vcTag" => "Alt-tag"
        );

        /* Array for all slider rows */
        $arrSliders = array();

        /* List orgs and set editing options */
        foreach ($slider->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iSlideID=" . $arrValues["iSlideID"], "eye") .
                getIcon("?mode=edit&iSlideID=" . $arrValues["iSlideID"], "pencil") .
                getIcon("", "trash", "Slet slider", "remove(" . $arrValues["iSlideID"] . ")");

            /* Add value row to arrUsers */
            $arrSliders[] = $arrValues;


        }

        /* Call list presenter object with columns (arrColumns) and rows (arrsliders) */
        $p = new listPresenter($arrColumns, $arrSliders);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iSlideID = filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iSlideID=" . $iSlideID, "Edit slider", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $slider = new slider();
        $slider->getImage_slider($iSlideID);


        $arrValues = get_object_vars($slider);


        $presenter = new listPresenter($slider->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iSlideID = (int)filter_input(INPUT_GET, "iSlideID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iSlideID=" . -1, "New", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $slider = new slider();

        /* Get org if state = update */
        if ($iSlideID > 0) {
            $slider->getImage_slider($iSlideID);
        }

        /* Get property values */
        $arrValues = get_object_vars($slider);


        /* Create presenter instance and set form */
        $form = new formpresenter($slider->arrLabels, $slider->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $slider = new slider();


        foreach ($slider->arrFormElms as $field => $arrTypes) {
            $slider->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iSlideID = $slider->save();
        header("Location: ?mode=details&iSlideID=" . $iSlideID);
        break;

    case "DELETE":
        $slider = new slider();
        $slider->iSlideID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $slider->delete($iSlideID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/public_html/assets/js/ajaxFunctions.js"></script>
