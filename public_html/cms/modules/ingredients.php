<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "ingredients";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iIngredientsID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $ingredients = new ingredients();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcTitle" => "Ingredient Name",
        );

        /* Array for all ingredients rows */
        $arrIngredients = array();

        /* List orgs and set editing options */
        foreach ($ingredients->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iIngredientsID=" . $arrValues["iIngredientsID"], "eye") .
                getIcon("?mode=edit&iIngredientsID=" . $arrValues["iIngredientsID"], "pencil") .
                getIcon("", "trash", "Slet ingredients", "remove(" . $arrValues["iIngredientsID"] . ")");

            /* Add value row to arrUsers */
            $arrIngredients[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arringredientss) */
        $p = new listPresenter($arrColumns, $arrIngredients);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iIngredientsID = filter_input(INPUT_GET, "iIngredientsID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iIngredientsID=" . $iIngredientsID, "Edit ingredients", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $ingredients = new ingredients();
        $ingredients->getIngredients($iIngredientsID);


        $arrValues = get_object_vars($ingredients);


        $presenter = new listPresenter($ingredients->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iIngredientsID = (int)filter_input(INPUT_GET, "iIngredientsID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iIngredientsID=" . -1, "New", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $ingredients = new ingredients();

        /* Get org if state = update */
        if ($iIngredientsID > 0) {
            $ingredients->getIngredients($iIngredientsID);
        }

        /* Get property values */
        $arrValues = get_object_vars($ingredients);


        /* Create presenter instance and set form */
        $form = new formpresenter($ingredients->arrLabels, $ingredients->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $ingredients = new ingredients();


        foreach ($ingredients->arrFormElms as $field => $arrTypes) {
            $ingredients->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iIngredientsID = $ingredients->save();
        header("Location: ?mode=details&iIngredientsID=" . $iIngredientsID);
        break;

    case "DELETE":
        $ingredients = new ingredients();
        $ingredients->iIngredientsID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $ingredients->delete($iIngredientsID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/public_html/assets/js/ajaxFunctions.js"></script>
