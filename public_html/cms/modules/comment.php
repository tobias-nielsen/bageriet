<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "comment";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iCommentID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $comment = new comment();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcName" => "Name",
        );

        /* Array for all comment rows */
        $arrcomments = array();

        /* List orgs and set editing options */
        foreach ($comment->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iCommentID=" . $arrValues["iCommentID"], "eye") .
                getIcon("?mode=edit&iCommentID=" . $arrValues["iCommentID"], "pencil") .
                getIcon("", "trash", "Slet comment", "remove(" . $arrValues["iCommentID"] . ")");

            /* Add value row to arrUsers */
            $arrcomments[] = $arrValues;


        }

        /* Call list presenter object with columns (arrColumns) and rows (arrcomments) */
        $p = new listPresenter($arrColumns, $arrcomments);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iCommentID = filter_input(INPUT_GET, "iCommentID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iCommentID=" . $iCommentID, "Edit comment", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $comment = new comment();
        $comment->getcomment($iCommentID);


        $arrValues = get_object_vars($comment);


        $presenter = new listPresenter($comment->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iCommentID = (int)filter_input(INPUT_GET, "iCommentID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=details", "Details", "btn-primary");
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iCommentID=" . -1, "New", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $comment = new comment();

        /* Get org if state = update */
        if ($iCommentID > 0) {
            $comment->getcomment($iCommentID);
        }

        /* Get property values */
        $arrValues = get_object_vars($comment);


        $strSelect = "SELECT iProductID, vcTitle FROM product WHERE iDeleted = 0 ORDER BY vcTitle";
        $arrOrgs = $db->_fetch_array($strSelect);
        /* Add a default value to the selectbox */
        array_unshift($arrOrgs, array("iProductID" => 0, "vcTitle" => "Vælg Produkt"));

        $arrValues["iProductID"] = formpresenter::inputSelect("iProductID", $arrOrgs, $comment->iProductID);


        /* Create presenter instance and set form */
        $form = new formpresenter($comment->arrLabels, $comment->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $comment = new comment();


        foreach ($comment->arrFormElms as $field => $arrTypes) {
            $comment->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iCommentID = $comment->save();
        header("Location: ?mode=details&iCommentID=" . $iCommentID);
        break;

    case "DELETE":
        $comment = new comment();
        $comment->iCommentID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $comment->delete($iCommentID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
