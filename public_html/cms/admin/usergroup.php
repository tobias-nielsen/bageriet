<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";

$mode = setMode();

$strModuleName = "usergroup";

switch (strtoupper($mode)) {

    case "LIST";
        $strModuleMode = "Overview";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iGroupID=-1')", "btn-success");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        $usergroup = new usergroup();

        /* Array with fields and friendly names for list purposes*/
        $arrColumns = array(
            "opts" => "Options",
            "vcGroupName" => "Group Name",
            "vcRoleName" => "Role Name",
            "txDesc" => "Description",
        );

        /* Array for all usergroup rows */
        $arrUserGroups = array();

        /* List orgs and set editing options */
        foreach ($usergroup->getlist() as $key => $arrValues) {
            $arrValues["opts"] = getIcon("?mode=details&iGroupID=" . $arrValues["iGroupID"], "eye") .
                getIcon("?mode=edit&iGroupID=" . $arrValues["iGroupID"], "pencil") .
                getIcon("", "trash", "Slet usergroup", "remove(" . $arrValues["iGroupID"] . ")");

            /* Add value row to arrUsers */
            $arrUserGroups[] = $arrValues;
        }

        /* Call list presenter object with columns (arrColumns) and rows (arrUserGroups) */
        $p = new listPresenter($arrColumns, $arrUserGroups);
        echo $p->presentlist();

        sysFooter();
        break;

    case "DETAILS";
        $iGroupID = filter_input(INPUT_GET, "iGroupID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = "Details";

        sysHeader();

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Overview", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iGroupID=" . $iGroupID, "Edit usergroup", "btn-success");

        echo textpresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);


        $usergroup = new usergroup();
        $usergroup->getGroup($iGroupID);


        $arrValues = get_object_vars($usergroup);

        /*Converts date/stamp to readable date */
        $arrValues["daCreated"] = date2local($arrValues["daCreated"]);


        $presenter = new listPresenter($usergroup->arrLabels, $arrValues);
        echo $presenter->presentdetails();


        sysFooter();
        break;

    case "EDIT";
        $iGroupID = (int)filter_input(INPUT_GET, "iGroupID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Edit";
        sysHeader();
        /* Set array button panel */
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButton("button", "Details", "getUrl('?mode=details&iGroupID=" . $iGroupID . "'),");
        $arrButtonPanel[] = getButton("button", "New", "getUrl('?mode=edit&iGroupID=-1')");
        $arrButtonPanel[] = getButton("button", "Overview", "getUrl('?mode=list')");
        /* Call static panel with title and button options */
        echo textPresenter::presentpanel($strModuleName, $strModuleMode, $arrButtonPanel);

        /* Create class instance and set current org */
        $usergroup = new usergroup();

        /* Get org if state = update */
        if ($iGroupID > 0) {
            $usergroup->getGroup($iGroupID);
        }

        /* Get property values */
        $arrValues = get_object_vars($usergroup);


        /* Create presenter instance and set form */
        $form = new formpresenter($usergroup->arrLabels, $usergroup->arrFormElms, $arrValues);
        echo $form->presentForm();

        sysFooter();
        break;

    case "SAVE":
        $iGroupID = (int)filter_input(INPUT_GET, "iGroupID", FILTER_SANITIZE_NUMBER_INT);

        $usergroup = new usergroup();

        foreach ($usergroup->arrFormElms as $field => $arrTypes) {
            $usergroup->$field = filter_input(INPUT_POST, $field, $arrTypes[1], getDefaultValue($arrTypes[3]));
        }

        /* Save method*/
        $iGroupID = $usergroup->save();
        header("Location: ?mode=details&iGroupID=" . $iGroupID);
        break;

    case "DELETE":
        $usergroup = new usergroup();
        $usergroup->iGroupID = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $usergroup->delete($iGroupID);
        header("Location: ?mode=list");
        break;


}

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/footer.php"; ?>
<script src="/assets/js/ajaxFunctions.js"></script>
