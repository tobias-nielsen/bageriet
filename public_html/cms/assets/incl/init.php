<?php

// Global classes
date_default_timezone_set("Europe/Copenhagen");
setlocale(LC_ALL, "danish");

define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING));
define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");


require_once COREPATH . 'functions.php';
require_once COREPATH . 'classes/classloader.php';


// Classloader & DB
$className = new classloader();
$db = new dbconf();

//$user = new user(); // new user = object, $user now becomes the variable holding the object

// Login Authenticator
$auth = new auth();
$auth->authentificate();

//($auth->user->admin === 0);
//if (!isset($auth->user->admin)) $auth->user->admin = 0;

if (!isset($auth->user->admin)) {
    echo $auth->loginform(auth::ERR_NOACCESS);
    exit();
}



