<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CMS LOGIN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="/cms/assets/css/login.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Montserrat|Source+Sans+Pro' rel='stylesheet'
          type='text/css'>

</head>
<body id="login">
<div class="container">
    <div class="row">
        <div class="col-xs-4 col-xs-offset-4">
            <div class="card card-container">
                <div id="errormsg" class="text-danger text-center">@ERRORMSG@</div>
                <h3>WEBROOT CMS<i class="fa fa-lg fa-sign-in pull-right" aria-hidden="true"></i></h3>
                <p>Your new content management system</p>
                <form class="form-signin" id="loginform" method="post" autocomplete="off">
                    <span id="reauth-email" class="reauth-email"></span>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                            <input type="text" id="username" name="username" class="form-control"
                                   placeholder="Indtast brugernavn" required autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                            <input type="password" id="password" name="password" class="form-control"
                                   placeholder="Indtast password" required>
                        </div>
                    </div>
                    <button class="btn btn-lg btn-block login-btn btn-signin" type="submit">Sign in</button>
                </form><!-- /form -->
                <a href="#" class="forgot-password">
                    Forgot password?
                </a>
            </div><!-- /card-container -->
        </div>
    </div><!-- /container -->
</div>


<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>