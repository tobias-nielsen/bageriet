$(document).ready(function () {
    var url = window.location;
    // Will only work if string in href matches with location
    $('ul.nav a[href="' + url + '"]').parent().addClass('active');

    // Will also work for relative and absolute hrefs
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('active').parent().parent().addClass('active');


    $('.summernote').summernote({
        fontNames: [
            'Open Sans'
        ]
    });

    $('.texteditor').summernote({
        height: 300,
        lang: 'da-DK',
        toolbar: [
            ['mybutton', ['hello']],
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'color', 'clear']],
            ['fontsize', ['fontsize']],
            ['fontname', ['fontname']],
            ['para', ['ol', 'ul', 'paragraph']],
            ['insert', ['link', 'picture', 'video']],
            ['table', ['table']],
            ['misc', ['fullscreen', 'codeview', 'help']]
        ],
        fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New',
            'Helvetica Neue', 'Helvetica', 'Impact', 'Lucida Grande', 'Open Sans',
            'Rock Salt', 'Tahoma', 'Times New Roman', 'Verdana'],
        fontNamesIgnoreCheck: ['Open Sans', 'Rock Salt']
    });

});

/**
 * Function to check if username is taken
 * Displays a message if Username is Available or not.
 */

$(function () {
    $("[data-check-username]").bind("blur", function () {
        $("#loader").show();
        $.ajax({
            url: "assets/scripts/checkuser.php",
            data: 'vcUserName=' + $("#vcUserName").val(),
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    $("#user-availability-status")
                        .html("Username Not Available.")
                        .removeClass('available')
                        .addClass('not-available');

                } else {
                    $("#user-availability-status")
                        .html("Username Available.")
                        .removeClass('not-available')
                        .addClass('available');
                }
                $("#loader").hide();
            },
            error: function () {
            }
        });
    });
});


function getUrl(strUrl) {
    document.location.href = strUrl;
}


function remove(id, mode, text) {
    mode = mode || "?mode=delete&id=" + id;
    text = text || "Vil du slette denne record";
    if (confirm(text)) {
        document.location.href = mode;
    }
}


function goback() {
    window.history.back();
}




/**
 * File upload wizard
 * 
 */

$(document).ready(function () {
    //Initialize tooltips
    $('.nav-tabs > li a[title]').tooltip();

    //Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

        var $target = $(e.target);

        if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);

    });
    $(".prev-step").click(function (e) {

        var $active = $('.wizard .nav-tabs li.active');
        prevTab($active);

    });
});

function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
}
function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
}

