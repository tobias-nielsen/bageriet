<?php
/**
 * Created by PhpStorm.
 * User: tn116
 * Date: 26-09-2017
 * Time: 12:49
 */

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/header.php";
        ?>

        <div class="container margin-bot-5percent">
            <h1 class="headline text-center margin-top-5percent margin-bot-5percent">Register din bruger</h1>
            <form method="POST" action="assets/scripts/register.php" id="" class="form-horizontal">
                <fieldset>
                    <div class='form-group row rowfix' data-group='vcUserName'>
                        <label class="col-md-2 col-form-label required" for="username">Username:</label>
                        <div class='col-md-9'>
                            <span id="user-availability-status"></span>

                            <input data-check-username class="form-control" name="vcUserName" type="text" id="vcUserName">
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcPassword'>
                        <label class='col-md-2 col-form-label required'>Adgangskode:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='password' name='vcPassword' id='password' data-required="1"
                                   data-validate="password" value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcFirstName'>
                        <label class='col-md-2 col-form-label '>Fornavn:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' name='vcFirstName' id='vcFirstName' data-required="1"
                                   data-validate="validText"
                                   value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcLastName'>
                        <label class='col-md-2 col-form-label '>Efternavn:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' name='vcLastName' id='vcLastName' data-required="1"
                                   data-validate="validText"
                                   value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcAddress'>
                        <label class='col-md-2 col-form-label '>Adresse:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' name='vcAddress' id='vcAddress' data-required="1" value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='iZip'>
                        <label class='col-md-2 col-form-label '>Postnummer:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' name='iZip' id='iZip' value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcCity'>
                        <label class='col-md-2 col-form-label '>By:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' name='vcCity' id='vcCity' value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcEmail'>
                        <label class='col-md-2 col-form-label '>Email:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='email' name='vcEmail' id='vcEmail' data-required="1"
                                   data-validate="validemail" value=''>
                        </div>
                    </div>
                    <div class='form-group row rowfix' data-group='vcPhone1'>
                        <label class='col-md-2 col-form-label '>Telefon:</label>
                        <div class='col-md-9'>
                            <input class='form-control' type='text' data-required="1" name='vcPhone1' id='vcPhone1' value=''>
                        </div>
                    </div>
                    <div class='buttonpanel btn-left'>
                        <button type="button" class="btn btn-success" onclick="validate(this.form)">Gem</button>
                    </div>
                </fieldset>
            </form>
        </div>



<?php
        require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/footer.php";