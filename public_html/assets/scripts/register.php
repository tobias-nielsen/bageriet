<?php require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$user = new user();


$vcUserName = filter_input(INPUT_POST, "vcUserName", FILTER_SANITIZE_STRING);
$vcPassword = filter_input(INPUT_POST, "vcPassword", FILTER_SANITIZE_STRING);
$vcFirstName = filter_input(INPUT_POST, "vcFirstName", FILTER_SANITIZE_STRING);
$vcLastName = filter_input(INPUT_POST, "vcFirstName", FILTER_SANITIZE_STRING);
$vcAddress = filter_input(INPUT_POST, "vcAddress", FILTER_SANITIZE_STRING);
$iZip = filter_input(INPUT_POST, "iZip", FILTER_VALIDATE_INT);
$vcCity = filter_input(INPUT_POST, "vcCity", FILTER_SANITIZE_STRING);
$vcEmail = filter_input(INPUT_POST, "vcEmail", FILTER_SANITIZE_STRING);
$vcPhone = filter_input(INPUT_POST, "vcPhone", FILTER_VALIDATE_INT);


$params = array(
    $vcUserName,
    $user->hashPassword($vcPassword),
    $vcFirstName,
    $vcLastName,
    $vcAddress,
    $iZip,
    $vcCity,
    $vcEmail,
    $vcPhone,
    time()
);


$sql = "INSERT INTO user (" .
    "vcUserName, " .
    "vcPassword, " .
    "vcFirstName, " .
    "vcLastName, " .
    "vcAddress, " .
    "iZip, " .
    "vcCity, " .
    "vcEmail, " .
    "vcPhone1, " .
    "daCreated) " .
    "VALUES(?,?,?,?,?,?,?,?,?,?)";


$db->_query($sql, $params);

$uid = $db->_getinsertid();
$gparams = array($uid, 3);
$strInsert = "INSERT INTO usergrouprel(iUserID, iGroupID) VALUES(?,?)";
$db->_query($strInsert, $gparams);


header('location: /index.php');