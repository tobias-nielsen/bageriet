<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


?>
    <div class="container-fluid padding-zero">
        <footer class="text-center padding-zero">
            <div class="footer-logo"><h1 class="logo">Bageriet</h1>
                <p>Lorem ipsum dolor sit amet, suscipit ante, urna vel vitae sodales tellus eu, nec lectus proin magna velit</p>
            </div>
            <div class="copyright"><?php copyrightFirm() ?> </div>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/assets/js/functions.js"></script>
    <script src="/assets/js/validate.js"></script>
    <script src="/assets/js/ajaxFunctions.js"></script>

    </body>
    </html>

<?php
$db->_close();