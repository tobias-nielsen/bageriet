<?php

// Global classes
date_default_timezone_set("Europe/Copenhagen");
setlocale(LC_ALL, "danish");

define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING) . "");
define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");


//require_once COREPATH . 'constants.php';
require_once COREPATH . 'functions.php';
require_once COREPATH . 'classes/classloader.php';


// Classloader
$className = new classloader();
$db = new dbconf();

$auth = new auth();
$auth->iShowLoginForm = 0; //Only for extranet purposes
$auth->authentificate();
