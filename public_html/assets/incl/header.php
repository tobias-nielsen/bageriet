<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

?>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php projectName() ?></title>
    <link rel="icon" type="image/png" href="images/favicon-32x32.png">
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet" type="text/css"/>


</head>
<body>
<div class="container-fluid padding-zero">
    <header role="banner">
        <nav id="navbar-primary" class="navbar" role="navigation">
            <div class="container-fluid padding-zero">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#navbar-primary-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse padding-zero" id="navbar-primary-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="index.php">Forside</a></li>
                        <li><a href="product.php">Produkter</a></li>

                        <li><a href="index.php"><h1 class="logo">bageriet</h1></li>
                        <li><a href="contact.php">Kontakt</a></li>
                        <?php if ($auth->checkSession()) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle"
                                   data-toggle="dropdown"><?php echo $auth->user->vcFirstName . "&nbsp;" . $auth->user->vcLastName ?>
                                    <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="?action=logout" class=""> Log Ud</a>
                                    </li>

                                </ul>

                            </li>

                        <?php } else { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login <span
                                            class="caret"></span></a>
                                <ul id="login-dp" class="dropdown-menu">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <form class="form form-signin" id="login-form" method="post"
                                                      autocomplete="off"
                                                "
                                                accept-charset="UTF-8" id="login-nav">
                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputEmail2">Email
                                                        address</label>
                                                    <input type="text" class="form-control" id="username"
                                                           name="username" placeholder="Brugernavn" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                    <input type="password" class="form-control"
                                                           id="password" name="password" placeholder="Password"
                                                           required>
                                                    <div class="help-block text-right"><a href="">Forget the password
                                                            ?</a></div>
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-block">Login
                                                    </button>
                                                </div>
                                                </form>
                                            </div>
                                            <div class="bottom text-center">
                                                Har du ikke en bruger?<a href="register.php?mode=register"><br><b>Tilmeld
                                                        dig nu</b></a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                        ?>
                        <li>
                            <form action="search.php" method="get" id="search-field">
                                <input type="search" placeholder="Search" name="keyword">
                                <input hidden type="submit">
                            </form>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header><!-- header role="banner" -->