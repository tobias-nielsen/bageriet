/**
 * post & display comment
 */



$(document).ready(function () {
    //alert("shit works");

    $("#submit_comment").click(function () {
        var product = $("#iProductID").val();
        var name = $("#vcName").val();
        var comment = $("#txContent").val();


        // Callback
        $.ajax({
            url: "assets/scripts/comment.php",
            type: "POST",
            data: {

                done: 1,
                "iProductID": product,
                "vcName": name,
                "txContent": comment,
            },
            success: function (data) {
                //console.log("succes worked");
                displayComment();
                $("#iProductID").val();
                $("#vcName").val();
                $("#txContent").val();


                //alert("success works??");
            }
        })
    });


});

/**
 * Function to add active class
 * on active URL when clicked
 */

$(document).ready(function () {
    $('.nav li a').click(function (e) {

        $('.nav li.active').removeClass('active');

        var $parent = $(this).parent();
        $parent.addClass('active');
        //e.preventDefault();
    });
});

function displayComment() {
    $.ajax({

        url: "assets/scripts/comment.php",
        type: "POST",
        data: {
            "display": 1
        },

        success: function (d) {
            $("#display_comments").html(d);
            console.log("displayed comment worked");
        }

    });
}


/**
 * function to check username exists
 */

$(function () {
    $("[data-check-username]").bind("blur", function () {
        $("#loader").show();
        $.ajax({
            url: "assets/scripts/checkuser.php",
            data: 'vcUserName=' + $("#vcUserName").val(),
            type: "POST",
            success: function (data) {
                if (data == "1") {
                    $("#user-availability-status")
                        .html("Username Not Available.")
                        .removeClass('available')
                        .addClass('not-available');

                } else {
                    $("#user-availability-status")
                        .html("Username Available.")
                        .removeClass('not-available')
                        .addClass('available');
                }
                $("#loader").hide();
            },
            error: function () {
            }
        });
    });
});


$(".bread-cat").on("click", function () {

    $(".bread-cat").removeClass("cat-active");
    $(this).addClass("cat-active");

    if ($(".bread-cat").hasClass("cat-active")) {

        var data = $(this).data("cat");

        var target = $("[data-parent]");

        var wrap = $(".cat-tx-wrap[data-catwrap='" + data + "'");

        //console.log(wrap.data());

        $(".cat-tx-wrap").css("display", "none");
        wrap.css("display", "block");

        target.css("display", "none");
        $("[data-parent='" + data + "']").css("display", "block");

        console.log($("[data-parent='" + data + "']"));
    }


});