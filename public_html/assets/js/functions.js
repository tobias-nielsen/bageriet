/* VALIDATION */

$(document).ready(function () {
    $('form[data-validate]').children("input[type=text],textarea").attr('data-valid', false).addClass('invalid f-pristine').bind("input change keyup", function () { //targets form children, adds classes, listens to inputs, change and keyup,
        $(this).validate();
        $(this).addClass('f-dirty').removeClass('f-pristine'); //if not valid, add f-dirt class, which can be costumized to fx have red border.
        if ($('[data-valid=false]').length > 0) {
            $('[data-validate] button').attr('disabled', true); //sets button to be disabled City default.
        }
        else {
            $('[data-validate] button').removeAttr('disabled'); //removes attr disabled from button, to enable it if criteria is met
        }
    });
    $.prototype.validate = function () { //custom Jque to easier call.
        var _ = $(this),
            type = _.attr('data-type'),
            len = _.attr('data-length'),
            val = _.val(),
            valid = [];
        switch (type) { // switch to control validations
            case 'name':
                valid[0] = !/[0-9]/.test(val); //! reverses that requirement for numbers, so it's only text.
                break;                         //breaks out of case to continue with next task
            case 'email':
                valid[0] = (val.indexOf('@') > 0 && val.split('@').length == 2 && val.lastIndexOf('.') < val.length - 1 && val.indexOf('@') < val.lastIndexOf('.') + 1); //Email validation has to be atleast 1 @, has to be text on both sides, . has to be after @, . can't be the last
                break;
            case 'number':
                valid[0] = /[0-9]/.test(val);  // ensures only numbers can be validated
                break;
        }

        if (len) {
            if (val.length >= parseInt(len.split(' ')[0]) && val.length <= parseInt(len.split(' ')[1])) { //parseint turns the "text" in number into numbers so they can be used and calculated.
                valid[1] = true;
            }
            else {
                valid[1] = false;
            }
        }
        if (valid.indexOf(false) != -1) {
            _.attr('data-valid', false).addClass('invalid').removeClass('valid'); // if data-valid is false, add class invalid, and remove the valid class.
            return false;
        }
        else {
            _.attr('data-valid', true).addClass('valid').removeClass('invalid'); // opposit of above, if data-valid is true, removed invalid.
        }
    }
});



/*  BUTTON FUNCTIONS  */

function goback() {
    window.history.back();
}

function remove(id, mode, text) {
    mode = mode || "?mode=delete&id=" + id;
    text = text || "Vil du slette denne record";
    if (confirm(text)) {
        document.location.href = mode;
    }


}

